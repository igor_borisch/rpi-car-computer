#!/usr/bin/python
# -*- coding: utf-8 -*-

import os
from Tkinter import Tk,Button
import sys 
def ProcExit():
    sys.exit(0)
def ProcOBDGL():
    os.system('/usr/bin/python /OBDGL/myOBD.py')
def ProcACCEL():
    os.system('/usr/bin/python /OBDGL/accelerate6.py')
def ProcSetForm():
    os.system('/usr/bin/python /OBDGL/SetSystemTime.py')
os.chdir('/OBDGL')
root=Tk()
root.configure(background='black',cursor='none')
#root.geometry("480x320")
root.attributes('-fullscreen', True)
Button_Exit=Button(root,text='Exit',width=14,height=5,bg='black',fg='red',font='arial 18',command=ProcExit)
Button_Exit.grid(row=0, column=0)
Button_ODBGL=Button(root,text='OBDGL',width=14,height=5,bg='black',fg='red',font='arial 18',command=ProcOBDGL)
Button_ODBGL.grid(row=0, column=1)
Button_ACCEL=Button(root,text='ACCEL',width=14,height=5,bg='black',fg='red',font='arial 18',command=ProcACCEL)
Button_ACCEL.grid(row=1, column=0)
Button_Time=Button(root,text='Set Time',width=14,height=5,bg='black',fg='red',font='arial 18',command=ProcSetForm)
Button_Time.grid(row=1, column=1)
root.mainloop()
